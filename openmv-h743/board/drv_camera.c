#include <rtthread.h>
#include <rtdevice.h>

#include <rtdbg.h>

#ifndef CAMERA_BUSNAME
#define CAMERA_BUSNAME "camif0"
#endif

extern int ov2640_probe(rt_camif_t *ci, const struct video_ops **ops, uint32_t *id);
extern int ov7725_probe(rt_camif_t *ci, const struct video_ops **ops, uint32_t *id);

static int camera_init(void)
{
    rt_camif_t *ci;
    const struct video_ops *ops = 0;
    rt_video_t *vid;
    int ret = -1;
    uint32_t id;

    ci = rt_camif_get(CAMERA_BUSNAME);
    if (!ci)
    {
        LOG_E("cambus: %s not found", CAMERA_BUSNAME);
        return -1;
    }

    rt_camif_power_reset(ci, 0, 0);

    if ((ret = ov7725_probe(ci, &ops, &id)) == 0)
    {
        goto _out;
    }

    if ((ret = ov2640_probe(ci, &ops, &id)) == 0)
    {
        goto _out;
    }

    LOG_I("camera id: %X", id);

_out:
    if ((ret == 0) && ops)
    {
        vid = rt_calloc(sizeof(rt_video_t), 1);
        if (!vid)
        {
            ret = -1;
            goto _err;
        }

        vid->ops = ops;
        vid->ci = ci;
        vid->chipid = id;

        ret = rt_video_register(vid, "video0", ci);
    }

_err:
    return ret;
}
INIT_COMPONENT_EXPORT(camera_init);
