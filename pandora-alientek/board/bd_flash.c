#include <rtthread.h>
#include <rtdevice.h>

/* 42-PE11 */
#define QSPI1_0_CS    "PE11"

extern int w25qxx_mtd_init(const char *mtd_name, const char * spi_device_name);
extern int stm32_qspi_bus_attach_device(int cspin, int active_level, const char *bus_name, const char *device_name);

int flash_init(void)
{
    int cspin;
    int ret;

    cspin = rt_pin_get(QSPI1_0_CS);
    if (cspin < 0)
    {
        return -1;
    }

    if (stm32_qspi_bus_attach_device(cspin, 0, "qspi1", "qspi1_0") != 0)
    {
        return -1;
    }

    ret = w25qxx_mtd_init("nor0", "qspi1_0");

    return ret;
}
INIT_DEVICE_EXPORT(flash_init);
