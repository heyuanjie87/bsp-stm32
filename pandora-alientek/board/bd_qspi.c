/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author            Notes
 * 2018-08-17     zylx              the first version
 */
 
#include <stm32l4xx.h>
#include <rtdevice.h>
#include <rthw.h>

#ifdef BSP_USING_QSPI

void HAL_QSPI_MspInit(QSPI_HandleTypeDef *hqspi)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    /* QSPI clock enable */
    __HAL_RCC_QSPI_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    /* QSPI1 GPIO Configuration
    PE10     ------> QSPI_BK1_CLK
    PE11     ------> QSPI_SOFT_CS
    PE12     ------> QSPI_BK1_IO0
    PE13     ------> QSPI_BK1_IO1
    PE14     ------> QSPI_BK1_IO2
    PE15     ------> QSPI_BK1_IO3
    */
    GPIO_InitStruct.Pin = GPIO_PIN_10 | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF10_QUADSPI;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
}

void HAL_QSPI_MspDeInit(QSPI_HandleTypeDef *hqspi)
{
    /* Peripheral clock disable */
    __HAL_RCC_QSPI_CLK_DISABLE();
    /* QSPI GPIO Configuration
    PE10     ------> QSPI_BK1_CLK
    PE11     ------> QSPI_SOFT_CS
    PE12     ------> QSPI_BK1_IO0
    PE13     ------> QSPI_BK1_IO1
    PE14     ------> QSPI_BK1_IO2
    PE15     ------> QSPI_BK1_IO3
    */
    HAL_GPIO_DeInit(GPIOE, GPIO_PIN_10 | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15);
}
#endif /*BSP_USING_QSPI*/
