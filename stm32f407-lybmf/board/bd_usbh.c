/*
 * File: stm32_usbh.c
 *
 * Change Logs:
 * Date           Author            Notes
 * 2017-10-30     ZYH            the first version
 */

#include <rtthread.h>
#include "board.h"
#include <stm32f4xx_hal_hcd.h>

#define OTG_FS_PORT 1

static void vbus_switch_on(void)
{
    /* USB_PWR_EN : PC14 PC15. */
    {
        GPIO_InitTypeDef  GPIO_InitStructure;

        __HAL_RCC_GPIOC_CLK_ENABLE();

        GPIO_InitStructure.Pin = GPIO_PIN_14 | GPIO_PIN_15;
        GPIO_InitStructure.Mode  = GPIO_MODE_OUTPUT_PP;
        GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStructure.Pull  = GPIO_PULLUP;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStructure);
        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_15, 0);
    }
}

void HAL_HCD_MspInit(HCD_HandleTypeDef *hcdHandle)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    
    vbus_switch_on();

    if (hcdHandle->Instance == USB_OTG_HS)
    {
        /**USB_OTG_FS GPIO Configuration
        PB12     ------> USB_OTG_HS_ID
        PB13     ------> USB_OTG_HS_VBUS
        PB14     ------> USB_OTG_HS_DM
        PB15     ------> USB_OTG_HS_DP
        */
        __HAL_RCC_GPIOB_CLK_ENABLE();
#ifdef USBH_USING_VBUS
        GPIO_InitStruct.Pin = GPIO_PIN_13;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
#endif
        GPIO_InitStruct.Pin = GPIO_PIN_12 | GPIO_PIN_14 | GPIO_PIN_15;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF12_OTG_HS_FS;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
        /* Peripheral clock enable */
        __HAL_RCC_USB_OTG_HS_CLK_ENABLE();
        /* Peripheral interrupt init */
        HAL_NVIC_SetPriority(OTG_HS_IRQn, 1, 3);
        HAL_NVIC_EnableIRQ(OTG_HS_IRQn);
    }

    if (hcdHandle->Instance == USB_OTG_FS)
    {
        /**USB_OTG_FS GPIO Configuration
        PA9     ------> USB_OTG_FS_VBUS
        PA10     ------> USB_OTG_FS_ID
        PA11     ------> USB_OTG_FS_DM
        PA12     ------> USB_OTG_FS_DP
        */
        __HAL_RCC_GPIOA_CLK_ENABLE();
#if 0//def USBH_USING_VBUS
        GPIO_InitStruct.Pin = GPIO_PIN_9;
        GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
#endif
        GPIO_InitStruct.Pin = GPIO_PIN_12 | GPIO_PIN_11 | GPIO_PIN_10 | GPIO_PIN_9;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
        GPIO_InitStruct.Alternate = GPIO_AF10_OTG_FS;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
        /* Peripheral clock enable */
        __HAL_RCC_USB_OTG_FS_CLK_ENABLE();
        /* Peripheral interrupt init */
        HAL_NVIC_SetPriority(OTG_FS_IRQn, 5, 0);
        HAL_NVIC_EnableIRQ(OTG_FS_IRQn);
    }    
}

void HAL_HCD_MspDeInit(HCD_HandleTypeDef *hcdHandle)
{
    if (hcdHandle->Instance == USB_OTG_FS)
    {
        /* Peripheral clock disable */
        __HAL_RCC_USB_OTG_FS_CLK_DISABLE();
        /**USB_OTG_FS GPIO Configuration
        PA9     ------> USB_OTG_FS_VBUS
        PA10     ------> USB_OTG_FS_ID
        PA11     ------> USB_OTG_FS_DM
        PA12     ------> USB_OTG_FS_DP
        */
#ifdef USBH_USING_VBUS
        HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9);
#endif
        HAL_GPIO_DeInit(GPIOA, GPIO_PIN_12 | GPIO_PIN_11);
        /* Peripheral interrupt Deinit*/
        HAL_NVIC_DisableIRQ(OTG_FS_IRQn);
    }
}


